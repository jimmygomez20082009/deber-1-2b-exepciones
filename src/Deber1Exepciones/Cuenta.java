package Deber1Exepciones;
import java.util.ArrayList;
public class Cuenta {
    String nombre;
    String Ncuenta;
    double interes;
    public Cuenta(String nombre, String Ncuenta) {
        this.nombre = nombre;
        this.Ncuenta = Ncuenta;
    } 
    public Cuenta() {
    }
}
class CuentaAhorros extends Cuenta{
    //static double saldo;
    double retiro;
    public CuentaAhorros(String nombre, String Ncuenta) {
        super(nombre, Ncuenta);
        //CuentaAhorros.saldo=saldo;
    }
    public CuentaAhorros() {
    }
    public double retiro(double saldo, double retiro, double interes) throws Deber1Exepciones.A_Excepcion{
        if (saldo > retiro) {
                    saldo -= retiro;
                    interes = (saldo * interes);
                } else {
                    try {
                        throw new A_Excepcion();
                    } catch (A_Excepcion e) {
                        System.err.println(e);
                    }
                }
        return (saldo +interes);
    }
}
class CuentaCheques extends Cuenta{
    public CuentaCheques(String nombre, String Ncuenta) {
        super(nombre, Ncuenta);
    }
    public CuentaCheques() {
    }
    public double retiro(double saldo, double retiro, double interes) throws Deber1Exepciones.A_Excepcion{
        if (saldo > retiro) {
                    saldo -= retiro;
                    interes = (saldo * interes);
                } else {
                    saldo -= retiro;
                    interes = (saldo * interes);
                }
        return (saldo +interes);
    }
}
class CuentaPlatino extends Cuenta{
    public CuentaPlatino(String nombre, String Ncuenta) {
        super(nombre, Ncuenta);
    }
    public CuentaPlatino() {
    }
    public double retiro(double saldo, double retiro) throws Deber1Exepciones.A_Excepcion{
        double interes =0.10;
        if (saldo > retiro) {
                    saldo -= retiro;
                    interes = (saldo * interes);
                } else {
                    saldo -= retiro;
                    interes = (saldo * interes);
                }
        return (saldo +interes);
    }
}
class Bancos{
    public static ArrayList <CuentaAhorros> cuenta = new ArrayList<>();
    public static ArrayList <CuentaCheques> cuenta1 = new ArrayList<>();
    public static ArrayList <CuentaPlatino> cuenta2 = new ArrayList<>();
    static CuentaAhorros cuentaAhorro = new CuentaAhorros("Jimmy", "1105");
    static CuentaCheques cuentaCheques = new CuentaCheques("Jimmy", "ji");
    static CuentaPlatino cuentaPlatino = new CuentaPlatino("Jimmy", "ji");
    public static void main(String[] args) throws A_Excepcion {
        try {
            System.out.println("Saldo de la cuenta con intereses: "+cuentaAhorro.retiro(100, 250, 0.10));
            System.out.println("Saldo de la cuenta con intereses: "+cuentaCheques.retiro(200, 400, 0.20));
            System.out.println("Saldo de la cuenta con intereses: "+cuentaPlatino.retiro(150, 200));
        } catch (A_Excepcion a_Excepcion) {
            System.err.println(a_Excepcion);
        }
    }
}